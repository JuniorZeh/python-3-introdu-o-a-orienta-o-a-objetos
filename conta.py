class Conta:
    #self é a referência que sabe encontrar o objeto que esta sendo construido na memória
    def __init__(self, numero, titular, saldo, limite):
        print("Construindo  objeto ... {}".format(self))
        self.numero = numero
        self.titular = titular
        self.saldo = saldo
        self.limite = limite